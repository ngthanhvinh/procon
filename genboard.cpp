#include <bits/stdc++.h>
using namespace std;

default_random_engine gen;
uniform_real_distribution<double> distribution(0.0, 1.0);

int get_rand() {
	double pt;
	for (int ntime = 0; ntime < 10; ++ntime) {
		pt = distribution(gen);
	}
	int val = rand() % 17;
	if (pt <= 0.1) return -val;
	else return val;
}

int main() {
	freopen("map.txt", "w", stdout);
	srand(time(NULL));
	ios_base::sync_with_stdio(false);
	cin.tie(0);

	int N = 12, M = 12;
	int value[15][15];

	cout << N << ' ' << M << ':';
	for (int i = 1; i <= N; ++i) {
		for (int j = 1; j <= M; ++j) {
			if (i > N / 2) {
				value[i][j] = value[N + 1 - i][j];
			} else if (j > M / 2) {
				value[i][j] = value[i][M + 1 - j];
			} else {
				value[i][j] = get_rand();
			}
		}
	}
	for (int i = 1; i <= N; ++i) {
		for (int j = 1; j <= M; ++j) {
			cout << value[i][j];
			if (j != M) cout << ' ';
			else cout << ':';
		}
	}
	int x = rand() % N, y = rand() % M;
	cout << x + 1 << ' ' << y + 1 << ':' << N - x << ' ' << M - y << endl;
}