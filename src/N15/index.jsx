import React from "react";
import n15 from "./lib/N15.js";
import "./index.less";

const WHITE = -1;
const BLUE = 0;
const ORANGE = 1;
const nameOfAgent = ["Agent0", "Agent1", "Agent2", "Agent3"];
const codeOfAgent = ["X1", "X2", "O1", "O2"];

async function pushMove(array, move, isNew = false) {
  array.push(move);
  try {
    await fetch("http://localhost:1236" + (isNew ? "/new" : "/"), {
      method: "post",
      mode: "cors",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ cmd: move })
    });
  } catch (e) {
    console.log("Cannot sync move to server: " + e.toString());
  }
}

function isAgent(where, agent) {
  for (let i = 0; i < agent.length; ++i) {
    if (where.x === agent[i].x && where.y === agent[i].y) return true;
  }
  return false;
}

function getName(where, agent) {
  for (let i = 0; i < agent.length; ++i) {
    if (where.x === agent[i].x && where.y === agent[i].y) return nameOfAgent[i];
  }
  return "";
}

function getCode(where, agent) {
  for (let i = 0; i < agent.length; ++i) {
    if (where.x === agent[i].x && where.y === agent[i].y) return codeOfAgent[i];
  }
  return "";
}

function getAgentID(where, agent) {
  for (let i = 0; i < 4; ++i) {
    if (where.x === agent[i].x && where.y === agent[i].y) return i;
  }
  return -1;
}

function tilePoint(c, state) {
  let ret = 0;
  for (let i = 0; i < state.N; ++i) {
    for (let j = 0; j < state.M; ++j) {
      if (state.color[i][j] == c) ret += state.value[i][j];
    }
  }
  return ret;
}

function surroundedPoint(c, state) {
  let ret = 0;
  let visited = [];
  for (let i = 0; i < state.N; ++i) {
    visited.push(Array(state.M).fill(false));
  }
  for (let i = 0; i < state.N; ++i) {
    for (let j = 0; j < state.M; ++j) {
      if (state.color[i][j] === c) continue;
      if (visited[i][j] === true) continue;
      // bfs
      let queue = [],
        ptr = 0,
        good = true,
        cur = 0;
      visited[i][j] = true;
      queue.push({ x: i, y: j });

      while (ptr < queue.length) {
        let x = queue[ptr].x,
          y = queue[ptr].y;
        cur += Math.abs(state.value[x][y]);
        ++ptr; // pop
        for (let dx = -1; dx <= +1; ++dx) {
          for (let dy = -1; dy <= +1; ++dy) {
            if (Math.abs(dx) + Math.abs(dy) != 1) continue;
            let nx = x + dx,
              ny = y + dy;
            if (nx < 0 || nx >= state.N || ny < 0 || ny >= state.M) {
              good = false;
              continue;
            }
            if (visited[nx][ny] === true || state.color[nx][ny] === c) continue;
            visited[nx][ny] = true;
            queue.push({ x: nx, y: ny });
          }
        }
      }

      if (good === true) ret += cur;
    }
  }

  return ret;
}

function numTiles(c, state) {
  let ret = 0;
  for (let i = 0; i < state.N; ++i) {
    for (let j = 0; j < state.M; ++j) {
      if (state.color[i][j] === c) ++ret;
    }
  }
  return ret;
}

class Square extends React.Component {
  render() {
    let className = "square";
    if (this.props.focus == true) {
      className += " focus";
    } else {
      if (this.props.type == BLUE) className += " blue";
      else if (this.props.type == ORANGE) className += " orange";
      if (this.props.isAgent) className += " agent";
    }
    return (
      <button
        className={className}
        title={this.props.title}
        onClick={() => this.props.onClick()}
        onContextMenu={() => this.props.onContextMenu()}
      >
        {this.props.endGame ? this.props.value : ""}
        <sub>{this.props.code}</sub>
      </button>
    );
  }
}

class ScoreBoard extends React.Component {
  render() {
    if (this.props.state.endGame === true) {
      return (
        <div className="log">
          <span
            style={
              this.props.state.myColor == BLUE
                ? { color: "rgb(31, 31, 231)" }
                : { color: "rgb(226, 89, 10)" }
            }
          >
            <strong>
              BLUE ⬛: ({numTiles(this.props.state.myColor, this.props.state)}{" "}
              tiles)
            </strong>
            <br />
            <br />
            <table className="scoreboard">
              <tr>
                <td>Tile Point</td>
                <td>{tilePoint(this.props.state.myColor, this.props.state)}</td>
              </tr>
              <tr>
                <td>Surrounded Point</td>
                <td>
                  {surroundedPoint(this.props.state.myColor, this.props.state)}
                </td>
              </tr>
              <tr>
                <td>Total Point</td>
                <td>
                  {tilePoint(this.props.state.myColor, this.props.state) +
                    surroundedPoint(this.props.state.myColor, this.props.state)}
                </td>
              </tr>
            </table>
          </span>
          <br />
          <span
            style={
              this.props.state.myColor == ORANGE
                ? { color: "rgb(31, 31, 231)" }
                : { color: "rgb(226, 89, 10)" }
            }
          >
            <strong>
              ORANGE ⬛: ({numTiles(
                this.props.state.myColor ^ 1,
                this.props.state
              )}{" "}
              tiles)
            </strong>
            <br />
            <br />
            <table className="scoreboard">
              <tr>
                <td>Tile Point</td>
                <td>
                  {tilePoint(this.props.state.myColor ^ 1, this.props.state)}
                </td>
              </tr>
              <tr>
                <td>Surrounded Point</td>
                <td>
                  {surroundedPoint(
                    this.props.state.myColor ^ 1,
                    this.props.state
                  )}
                </td>
              </tr>
              <tr>
                <td>Total Point</td>
                <td>
                  {tilePoint(this.props.state.myColor ^ 1, this.props.state) +
                    surroundedPoint(
                      this.props.state.myColor ^ 1,
                      this.props.state
                    )}
                </td>
              </tr>
            </table>
          </span>
        </div>
      );
    } else {
      return <div />;
    }
  }
}

const dx = [-1, -1, 0, +1, +1, +1, 0, -1, 0];
const dy = [0, +1, +1, +1, 0, -1, -1, -1, 0];

function whichDir(st, en) {
  for (let dir = 0; dir < 8; ++dir) {
    if (st.x + dx[dir] === en.x && st.y + dy[dir] === en.y) {
      return dir;
    }
  }
  return -1;
}

class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = { str: "", color: BLUE };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNextTurn = this.handleNextTurn.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleContextMenu = this.handleContextMenu.bind(this);
    this.handleAgentChange = this.handleAgentChange.bind(this);
    this.handleAgentSubmit = this.handleAgentSubmit.bind(this);
    this.handleEndGame = this.handleEndGame.bind(this);
  }
  handleChange(event) {
    if (event.target.name === "color") {
      if (event.target.value === "blue") {
        this.setState({ color: BLUE });
      } else {
        this.setState({ color: ORANGE });
      }
    } else {
      this.setState({ str: event.target.value });
    }
  }
  async handleSubmit(event) {
    this.props.state.isInput = false;
    this.props.state.myColor = this.state.color;

    // Decode the string
    let str = this.state.str;
    let line = str.split(/\r?\n/);
    pushMove(this.props.state.console, line[0], true);

    let arr = line[0].split(":");
    let tmp = arr[0].split(" ");
    this.props.state.N = parseInt(tmp[0]);
    this.props.state.M = parseInt(tmp[1]);

    this.props.state.value = [];
    for (let i = 1; i <= this.props.state.N; ++i) {
      let a = arr[i].split(" ");
      this.props.state.value.push(Array(0));
      for (let j = 0; j < a.length; ++j) {
        if (a[j] != "") {
          this.props.state.value[i - 1].push(parseInt(a[j]));
        }
      }
    }

    this.props.state.color = [];
    for (let i = 0; i < this.props.state.N; ++i) {
      this.props.state.color.push(Array(this.props.state.M).fill(-1));
    }

    tmp = arr[this.props.state.N + 1].split(" ");
    this.props.state.agent[0] = {
      x: parseInt(tmp[0]) - 1,
      y: parseInt(tmp[1]) - 1
    };
    this.props.state.agent[2] = {
      x: parseInt(tmp[0]) - 1,
      y: this.props.state.M - parseInt(tmp[1])
    }; // vertically symmetric
    tmp = arr[this.props.state.N + 2].split(" ");
    this.props.state.agent[1] = {
      x: parseInt(tmp[0]) - 1,
      y: parseInt(tmp[1]) - 1
    };
    this.props.state.agent[3] = {
      x: parseInt(tmp[0]) - 1,
      y: this.props.state.M - parseInt(tmp[1])
    }; // vertically symmetric

    let agent = this.props.state.agent;
    this.props.state.color[agent[0].x][agent[0].y] = this.props.state.myColor;
    this.props.state.color[agent[1].x][agent[1].y] = this.props.state.myColor;
    this.props.state.color[agent[2].x][agent[2].y] =
      this.props.state.myColor ^ 1;
    this.props.state.color[agent[3].x][agent[3].y] =
      this.props.state.myColor ^ 1;

    this.props.state.newAgent = this.props.state.agent.slice();
    event.preventDefault();
    this.state.str = "";
    this.props.move();

    for (let i = 1; i < line.length; ++i) {
      arr = line[i].split(" ");
      let id = parseInt(arr[1]);
      let dir = parseInt(arr[2]);

      if (parseInt(id) != id || parseInt(dir) != dir) continue;
      if (id >= 4 || id < 0) continue;
      if (dir < 0 || dir > 8) continue;

      if (arr[0] === "move") {
        this.props.state.focus = this.props.state.agent[id];
        await this.handleClick(
          this.props.state.focus.x + dx[dir],
          this.props.state.focus.y + dy[dir]
        );
        this.state.str = "";
      } else if (arr[0] === "remove") {
        this.props.state.focus = this.props.state.agent[id];
        await this.handleContextMenu(
          this.props.state.focus.x + dx[dir],
          this.props.state.focus.y + dy[dir]
        );
        this.state.str = "";
      }
    }
  }

  handleNextTurn() {
    this.props.state.nturn++;
    this.props.state.focus = null;
    this.props.move();
  }

  handleClick(x, y) {
    if (x < 0 || x >= this.props.state.N || y < 0 || y >= this.props.state.M)
      return;
    if (this.props.state.focus == null) {
      if (isAgent({ x: x, y: y }, this.props.state.agent)) {
        this.props.state.focus = { x: x, y: y };
        this.props.move();
      }
      return;
    } else if (this.props.state.focus.x == x && this.props.state.focus.y == y) {
      this.props.state.focus = null;
      this.props.move();
      return;
    }

    if (isAgent({ x: x, y: y }, this.props.state.agent)) {
      return;
    }

    let fx = this.props.state.focus.x;
    let fy = this.props.state.focus.y;
    if (Math.abs(fx - x) > 1 || Math.abs(fy - y) > 1) return;

    let id = getAgentID(this.props.state.focus, this.props.state.agent);
    let color = this.props.state.color.map(v => v.slice());

    pushMove(
      this.props.state.console,
      "move " + id + " " + whichDir({ x: fx, y: fy }, { x: x, y: y })
    ); // {whichAgent, whichDir}

    if (color[x][y] === WHITE || color[x][y] === color[fx][fy]) {
      color[x][y] = color[fx][fy]; // move
      this.props.state.agent[id] = { x: x, y: y };
    } else {
      color[x][y] = WHITE; // remove
    }
    this.props.state.focus = null;
    this.props.state.color = color.map(v => v.slice());
    this.props.move();
  }

  handleContextMenu(x, y) {
    // remove our cell
    if (x < 0 || x >= this.props.state.N || y < 0 || y >= this.props.state.M)
      return;
    if (this.props.state.focus == null) return;
    let fx = this.props.state.focus.x;
    let fy = this.props.state.focus.y;
    if (Math.abs(fx - x) > 1 || Math.abs(fy - y) > 1) return;

    if (isAgent({ x: x, y: y }, this.props.state.agent)) {
      return;
    }

    let id = getAgentID(this.props.state.focus, this.props.state.agent);
    let color = this.props.state.color.map(v => v.slice());

    if (color[x][y] === color[fx][fy]) {
      pushMove(
        this.props.state.console,
        "remove " + id + " " + whichDir({ x: fx, y: fy }, { x: x, y: y })
      ); // {whichAgent, whichDir}
      color[x][y] = WHITE; // remove
      this.props.state.focus = null;
      this.props.state.color = color.map(v => v.slice());
      this.props.move();
    }
    return;
  }

  handleAgentChange(event) {
    this.setState({ str: event.target.value });
  }

  async handleAgentSubmit(event) {
    if (event.which != 13) {
      // Enter
      return;
    }
    let str = this.state.str;
    let arr = str.split(" ");
    if (arr.length % 3 != 0) return;

    for (let i = 0; i < arr.length; i += 3) {
      let id = parseInt(arr[i + 1]);
      let dir = parseInt(arr[i + 2]);

      if (parseInt(id) != id || parseInt(dir) != dir) continue;
      if (id >= 4 || id < 0) continue;
      if (dir < 0 || dir > 8) continue;

      if (arr[i] === "move") {
        this.props.state.focus = this.props.state.agent[id];
        await this.handleClick(
          this.props.state.focus.x + dx[dir],
          this.props.state.focus.y + dy[dir]
        );
        this.state.str = "";
      } else if (arr[i] === "remove") {
        this.props.state.focus = this.props.state.agent[id];
        await this.handleContextMenu(
          this.props.state.focus.x + dx[dir],
          this.props.state.focus.y + dy[dir]
        );
        this.state.str = "";
      }
    }
  }

  handleEndGame() {
    this.props.state.endGame = true;
    console.log(this.props.state.endGame);
    this.props.move();
  }

  render() {
    if (this.props.state.isInput === true) {
      return (
        <div className="n15 log">
          <strong>Decode QRCode</strong>
          <form onSubmit={this.handleSubmit}>
            <textarea
              name="Text"
              cols="40"
              rows="7"
              onChange={this.handleChange}
            />
            <br />
            <input type="submit" value="Submit" />
          </form>

          <br />
          <form>
            <strong>My Color</strong> <br />
            <span style={{ color: "rgb(31, 31, 231)" }}> ⬛</span>
            <input
              type="radio"
              name="color"
              value="blue"
              checked={this.state.color === BLUE}
              onChange={this.handleChange}
            />
            <span style={{ color: "rgb(226, 89, 10)" }}> ⬛</span>
            <input
              type="radio"
              name="color"
              value="orange"
              checked={this.state.color === ORANGE}
              onChange={this.handleChange}
            />
          </form>
        </div>
      );
    }

    let N = this.props.state.N;
    let M = this.props.state.M;
    // build arrBoard
    let arrBoard = [];
    arrBoard.push(<span className="invisible" />);
    for (let i = 0; i < M; ++i) {
      arrBoard.push(<span className="invisible">{i + 1}</span>);
    }
    arrBoard.push(<br />);
    for (let i = 0; i < N; ++i) {
      arrBoard.push(<span className="invisible">{i + 1}</span>);
      for (let j = 0; j < M; ++j) {
        arrBoard.push(
          <Square
            type={WHITE}
            value={this.props.state.value[i][j]}
            isAgent={isAgent({ x: i, y: j }, this.props.state.agent)}
            type={this.props.state.color[i][j]}
            title={getName({ x: i, y: j }, this.props.state.agent)}
            code={getCode({ x: i, y: j }, this.props.state.agent)}
            focus={
              this.props.state.focus != null &&
              i === this.props.state.focus.x &&
              j === this.props.state.focus.y
                ? true
                : false
            }
            onClick={() => this.handleClick(i, j)}
            onContextMenu={() => this.handleContextMenu(i, j)}
            endGame={this.props.state.endGame}
          />
        );
      }
      arrBoard.push(<br />);
    }

    return (
      <div className="n15">
        <div className="log">
          <strong>Number of turns: {this.props.state.nturn}</strong>{" "}
          <button className="next" onClick={this.handleNextTurn}>
            Next Turn
          </button>
        </div>
        <div className="log">
          <button className="endgame" onClick={this.handleEndGame}>
            End Game
          </button>
        </div>
        <br />

        <div className="game-play">{arrBoard}</div>

        <ScoreBoard state={this.props.state} />
      </div>
    );
  }
}

export default Board;
