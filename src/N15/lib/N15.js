"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

const WHITE = -1;
const BLUE = 0;
const ORANGE = 1;

const N15 = {
  default() {
    return {
      isInput: true,
      N: 0,
      M: 0,
      value: [],
      color: [],
      agent: Array(4),
      myColor: BLUE,
      nturn: 0,
      newAgent: Array(4),
      focus: null,
      console: [],
      endGame: false
    };
  },

  actions: {
    async move(state) {
      return state;
    }
  },

  isValid(state) {
    return true;
    // nothing to do here ?
  },

  isEnding(state) {}
};

exports.default = N15;
