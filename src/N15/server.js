const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const expressLogger = require("morgan");
const http = require("http");
const cors = require("cors");
const fs = require("fs");

const PORT = process.env.PORT || 1236;

let app = express();
let httpServer = http.createServer(app);

// Parsers
app.use(cors());
app.use(expressLogger("dev"));
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(bodyParser.json());
app.use(cookieParser());

let allMoves = [];

async function writeMoves(req, res) {
  allMoves.push(req.body.cmd);
  console.log("pushed " + req.body.cmd);
  await new Promise(r => fs.writeFile("log.txt", allMoves.join("\n"), r));
  console.log("written log");
  res.json({});
}

app.use("/new", (req, res) => {
  console.log("new game");
  allMoves = [];
  writeMoves(req, res);
});

app.use("/", writeMoves);

httpServer.on("listening", () => {
  let addr = httpServer.address();
  let bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
  console.log("Listening on " + bind);
});

httpServer.listen(PORT);
