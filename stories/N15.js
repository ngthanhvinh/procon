import React from "react";
import { storiesOf } from "@storybook/react";
import { number, withKnobs } from "@storybook/addon-knobs";
import Board from "../src/N15/index.jsx";
import Game from "../src/N15/lib/N15.js";
import ReactGame from "react-gameboard/lib/component";
import Guide from "../src/N15/guide.jsx";

const N15 = ReactGame(Game);

storiesOf("Procon", module)
  .add("Custom", () => {
    const options = {
      range: true,
      step: 1,
      min: 4,
      max: 13
    };
    return (
      <N15 N={12} M={12}>
        <Board />
      </N15>
    );
  });
